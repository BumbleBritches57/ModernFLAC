#include "../Dependencies/libPCM/Dependencies/BitIO/libBitIO/include/CommandLineIO.h"
#include "../Dependencies/libPCM/Dependencies/BitIO/libBitIO/include/BitIO.h"
#include "../Dependencies/libPCM/Dependencies/BitIO/libBitIO/include/BitIOLog.h"
#include "../Dependencies/libPCM/libPCM/include/libPCM.h"

#include "../libModernFLAC/include/libModernFLAC.h"

#ifdef __cplusplus
extern "C" {
#endif
    
    enum ModernFLACCommandLineSwitchNames {
        Input                            = 0,
        Output                           = 1,
        Encode                           = 2,
        Optimize                         = 3,
        Subset                           = 4,
        Decode                           = 5,
        Help                             = 6,
        NumSwitches                      = 7,
    };
    
    CommandLineIO *SetModernFLACOptions(void) {
        CommandLineIO *CLI               = CommandLineIO_Init(NumSwitches);
        
        UTF8 ProgramName[]               = u8"ModernFLAC";
        UTF8 ProgramVersion[]            = u8"0.5.0";
        UTF8 ProgramAuthor[]             = u8"BumbleBritches57";
        UTF8 ProgramCopyright[]          = u8"2017+";
        UTF8 ProgramDescription[]        = u8"FLAC Encoder/Decoder Written in Modern C, and licensed premissively";
        UTF8 ProgramLicenseName[]        = u8"Revised BSD";
        UTF8 ProgramLicenseURL[]         = u8"https://tldrlegal.com/license/bsd-3-clause-license-(revised)";
        
        UTF8 InputSwitchName[]           = u8"Input";
        UTF8 InputSwitchDescription[]    = u8"Input file or stdin with: -";
        
        UTF8 OutputSwitchName[]          = u8"Output";
        UTF8 OutputSwitchDescription[]   = u8"Output file or stdout with: -";
        
        UTF8 EncodeSwitchName[]          = u8"Encode";
        UTF8 EncodeSwitchDescription[]   = u8"Encodes PCM or FLAC input to FLAC";
        
        UTF8 OptimizeSwitchName[]        = u8"Optimize";
        UTF8 OptimizeSwitchDescription[] = u8"Encode modifier; Optimizes the encoded FLAC file to be as small as possible";
        
        UTF8 SubsetSwitchName[]          = u8"Subset";
        UTF8 SubsetSwitchDescription[]   = u8"Encode modifier, limits Optimize; only allows encoding subset FLAC files";
        
        UTF8 DecodeSwitchName[]          = u8"Decode";
        UTF8 DecodeSwitchDescription     = u8"Decodes FLAC input to PCM";
        
        UTF8 HelpSwitchName[]            = u8"Help";
        UTF8 HelpSwitchDescription[]     = u8"Prints all of the available options, and their relationships";
        
        BitIOLog_SetProgramName(ProgramName);
        CLISetName(CLI, ProgramName);
        CLISetMinOptions(CLI, 3);
        
        CLISetAuthor(CLI, ProgramAuthor);
        CLISetVersion(CLI, ProgramVersion);
        CLISetCopyright(CLI, ProgramCopyright);
        CLISetDescription(CLI, ProgramDescription);
        CLISetLicense(CLI, PermissiveLicense, ProgramLicenseName, ProgramLicenseURL);
        
        CLISetSwitchName(CLI, Input, InputSwitchName);
        CLISetSwitchDescription(CLI, Input, InputSwitchDescription);
        CLISetSwitchArgumentType(CLI, Input, ArgumentIsAPath);
        
        CLISetSwitchName(CLI, Output, OutputSwitchName);
        CLISetSwitchDescription(CLI, Output, OutputSwitchDescription);
        CLISetSwitchArgumentType(CLI, Output, ArgumentIsAPath);
        
        CLISetSwitchName(CLI, Encode, EncodeSwitchName);
        CLISetSwitchDescription(CLI, Encode, EncodeSwitchDescription);
        CLISetSwitchArgumentType(CLI, Encode, ArgumentNotAllowed);
        CLISetSwitchMaxConcurrentSlaves(CLI, Encode, 2);
        
        CLISetSwitchName(CLI, Optimize, OptimizeSwitchName);
        CLISetSwitchDescription(CLI, Optimize, OptimizeSwitchDescription);
        CLISetSwitchAsSlave(CLI, Encode, Optimize);
        CLISetSwitchArgumentType(CLI, Encode, ArgumentNotAllowed);
        
        CLISetSwitchName(CLI, Subset, SubsetSwitchName);
        CLISetSwitchDescription(CLI, Subset, SubsetSwitchDescription);
        CLISetSwitchAsSlave(CLI, Encode, Subset);
        CLISetSwitchArgumentType(CLI, Encode, ArgumentNotAllowed);
        
        CLISetSwitchName(CLI, Decode, DecodeSwitchName);
        CLISetSwitchDescription(CLI, Decode, DecodeSwitchDescription);
        CLISetSwitchArgumentType(CLI, Decode, ArgumentIsAPath);
        
        CLISetSwitchName(CLI, Help, HelpSwitchName);
        CLISetSwitchDescription(CLI, Help, HelpSwitchDescription);
        CLISetSwitchArgumentType(CLI, Encode, ArgumentNotAllowed);
        
        return CLI;
    }
    
    void FLACDecodeFile(DecodeFLAC *Dec, CommandLineIO *CLI, BitBuffer *InputFLAC, BitBuffer *OutputPCM) {
        uint32_t FileMagic = ReadBits(BitIOMSByteFirst, BitIOLSBitFirst, InputFLAC, 32);
        
        if (FileMagic == FLACMagic) {
            for (size_t Byte = 4; Byte < GetBitInputFileSize(InputFLAC); Byte++) { // loop to decode file
                while (Dec->LastMetadataBlock == false) {
                    FLACParseMetadata(InputFLAC, Dec);
                }
                if (ReadBits(BitIOMSByteFirst, BitIOLSBitFirst, InputFLAC, 14) == FLACFrameMagic) {
                    FLACReadFrame(InputFLAC, Dec);
                }
            }
        } else {
            BitIOLog(BitIOLog_ERROR, __func__, u8"Not a FLAC file, magic was: 0x%X\n", FileMagic);
        }
    }
    
    void FLACEncodeFile(EncodeFLAC *Enc, CommandLineIO *CLI, BitBuffer *InputAudio, BitBuffer *OutputFLAC) {
        Enc->EncodeSubset = CLIGetOptionNum(CLI, Subset, 0, NULL);
        Enc->OptimizeFile = CLIGetOptionNum(CLI, Optimize, 0, NULL);
        // Start requesting PCM samples to encode into frames, given all PCM formats are interleaved, you'll need to handle that.
    }
    
    int main(int argc, const char *argv[]) {
        CommandLineIO *CLI = SetModernFLACOptions();
        
        ParseCommandLineArguments(CLI, argc, argv);
        BitInput           *BitI         = InitBitInput();
        BitOutput          *BitO         = InitBitOutput();
        BitBuffer          *InputFLAC    = InitBitBuffer(48);
        BitBuffer          *OutputFLAC   = InitBitBuffer(48);
        PCMFile            *PCM          = InitPCMFile();
        DecodeFLAC         *Dec          = InitDecodeFLAC();
        EncodeFLAC         *Enc          = InitEncodeFLAC();
        OpenCLIInputFile(InputFLAC, CLI, 0);
        OpenCLIOutputFile(BitO, CLI, 1);
        
        bool DecodeFLAC   = CLIGetNumMatchingOptions(CLI, Decode, 0, NULL);
        bool EncodeFLAC   = CLIGetNumMatchingOptions(CLI, Encode, 0, NULL);
        bool ReencodeFLAC = DecodeFLAC && EncodeFLAC == Yes ? Yes : No;
        bool SubsetFLAC   = CLIGetNumMatchingOptions(CLI, Subset, 0, NULL);
        
        // Find out if -d or -e was included on the command line
        if (DecodeFLAC == true || ReencodeFLAC == true) {
            if (ReadBits(BitIOMSByte, BitIOLSBit, InputFLAC, 32) == FLACMagic) {
                for (uint8_t Byte = 0; Byte < GetBitInputFileSize(InputFLAC); Byte++) {
                    if (PeekBits(BitIOMSByte, BitIOLSBit, InputFLAC, 14) == FLACFrameMagic) {
                        FLACReadFrame(InputFLAC, Dec);
                    } else {
                        FLACParseMetadata(InputFLAC, Dec);
                    }
                }
            }
            // Decode the file.
            // To decode we'll need to init the DecodeFLAC, and output the stuff to wav or w64
        } else if (Encode == true) {
            CLIGetOptionNum(CLI, Decode, 0, NULL);
            Enc->EncodeSubset = SubsetFLAC;
            Enc->OptimizeFile = OptimizeFLAC;
            IdentifyPCMFile(InputFLAC, PCM);
            EncodeFLACFile(PCM, BitO, Enc);
            
            // Encode the file to FLAC
            // ParseWAV and encode FLAC
        } else {
            // Reencode the input
        }
        
        CloseBitOutput(BitO);
        CloseBitBuffer(InputFLAC);
        ClosePCMFile(PCM);
        DeinitCommandLineIO(CLI);
        DeinitFLACDecoder(Dec);
        DeinitFLACEncoder(Enc);
        
        return EXIT_SUCCESS;
    }
    
#ifdef __cplusplus
}
#endif
