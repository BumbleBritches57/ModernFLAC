**ModernFLAC:**
========================
* ModernFLAC is a library for encoding, and decoding FLAC files written in Modern C.
* ModernFLAC uses HeaderDoc style formatting, which is compatible with Doxygen, to build the documentation call make with `make docs`

Before You Compile:
----------------------------
Before calling `make` be sure to call `cd WhereverYouPutModernFLAC` then `git submodule update --recursive --remote` to update it's dependencies.

Compiling:
----------
* The Makefile's default installation location is: `/usr/local/Packages/libModernFLAC`.
* You can change that by calling `make install` with: `PREFIX="/your/path/here" make install`, or just editing the makefile.
* The makefile by default builds the library as a static library, in release form, for the computer it's building on with `-march=native`.
* On my Mac, the ModernFLAC library is about 75kb, if that's too big for your use, enable link time optimization on your program to trim unused functions.

How To Use libModernFLAC:
-------------------------
In order to use libModernFLAC, you need to include libModernFLAC.h.

In order to decode a FLAC file, you need to call `DecodeFLACFile`, to encode call `EncodeFLACFile` and call include a FILE pointer to the uncompressed PCM (in WAV, W64, or AIFF format)

License:
-----------
ModernFLAC is released under the Simplified (3 clause) BSD license.
